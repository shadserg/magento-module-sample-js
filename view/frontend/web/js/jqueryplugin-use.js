define([
    'jquery',
    'underscore',
    'shadserg_samplejs_jqueryplugin'
], function ($, _) {
    'use strict';

    console.log('init jqueryplugin-use');

    return function (config, element) {
        console.log('execute jqueryplugin-use', config, element);

        //some code...

        var options = config.options ? config.options : {};
        _.defaults(options, {color: '#00f'});
        $(element).shadserg_samplejs_jqueryplugin(options);
    };
});
