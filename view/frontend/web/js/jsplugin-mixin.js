define([
    'mage/utils/wrapper'
], function (wrapper) {
    'use strict';

    return function (myplugin) {

        myplugin.prepareMessage = wrapper.wrapSuper(myplugin.prepareMessage, function (params) {
            var msg = '<wrap>\n';
            params.additional = 'jsplugin-mixin.js';
            msg += this._super(params);
            msg += '</wrap>\n';
            return msg;
        });

        return myplugin;
    };
});
