define([
    'underscore',
    'jquery'
], function (_, $) {
    'use strict';

    var plugin = {

        pluginName: 'jsplugin',
        counterInitElement: 0,
        counterOnClickElement: 0,

        initElement: function(config, element) {
            console.log(this.prepareMessage({
                method: 'initElement',
                counterInitElement: ++this.counterInitElement
            }), config, element);

            if (element) {
                $(element).on('click', this.onClickElement.bind(this));
            }
        },

        onClickElement: function() {
            console.log(this.prepareMessage({
                method: 'onClickElement',
                counterOnClickElement: ++this.counterOnClickElement
            }));
        },

        prepareMessage: function(params) {
            if (typeof params === 'undefined') {
                params = {};
            }
            _.defaults(params, {plugin: this.pluginName});
            var msg = '';
            _.each(params, function(value, key){
                msg += key + ': ' + value + '\n';
            });
            return msg;
        }

    };

    plugin.shadserg_samplejs_jsplugin = plugin.initElement;

    console.log('init ' + plugin.pluginName);

    return plugin;
});
