define([
    'jquery',
    'jquery/ui',
    'underscore',
    'shadserg_samplejs_jsplugin'
], function ($, jQueryUi, _, jsplugin) {
    'use strict';

    console.log('init jqueryplugin');

    $.widget('mage.shadserg_samplejs_jqueryplugin', {

        pluginName: 'jqueryplugin',
        counterOnClickElement: 0,

        options: {
            message: 'default message',
            color: '#f00'
        },

        /**
         * Bind click handler for closing the popup window and resize the popup based on the image size.
         * @private
         */
        _create: function () {
            console.log(this._prepareMessage({
                method: '_create',
                color: this.options.color
            }), this.element, this.options);

            this.element.css('background-color', this.options.color);
            this.element.on('click', this._onClickElement.bind(this));
        },

        _onClickElement: function () {
            console.log(this._prepareMessage({
                method: '_onClickElement',
                counterOnClickElement: ++this.counterOnClickElement
            }));
        },

        _prepareMessage: function (params) {
            if (typeof params === 'undefined') {
                params = {};
            }
            _.defaults(params, {
                plugin: this.pluginName,
                message: this.options.message
            });
            return jsplugin.prepareMessage(params);
        }
    });

    return $.mage.shadserg_samplejs_jqueryplugin;
});
