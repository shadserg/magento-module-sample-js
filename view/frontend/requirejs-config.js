var config = {
    map: {
        '*': {
            shadserg_samplejs_jsplugin: 'Shadserg_SampleJs/js/jsplugin',
            shadserg_samplejs_jqueryplugin: 'Shadserg_SampleJs/js/jqueryplugin',
            shadserg_samplejs_jqueryplugin_use: 'Shadserg_SampleJs/js/jqueryplugin-use'
        }
    },
    config: {
        mixins: {
            'Shadserg_SampleJs/js/jsplugin': {
                'Shadserg_SampleJs/js/jsplugin-mixin': true
            }
        }
    }
};
